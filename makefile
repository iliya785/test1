dirs = ./lib/testlib ./lib/dynlib ./src

all:
	for i in $(dirs);\
	do\
		cd $$i;\
		make;\
		cd $(shell pwd);\
	done


